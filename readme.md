# About
This script adds vhost block, updates your hosts file and restart your apache.

## Installation
This script requires a certain apache setup. Follow these instructions:

1. git clone git@gitlab.com:havber/simple-add-vhost.git

2. cd simple-add-vhost

3. sudo vi /private/etc/apache2/extra/httpd-vhosts.conf

    Legg til i toppen av filen:
    
    Include /private/etc/apache2/extra/sites/*.conf

4. sudo mkdir /private/etc/apache2/extra/sites

5. sudo cp _base_ez4.conf /private/etc/apache2/extra/sites/_base_ez4.conf

6. sudo cp _base_ez5.conf /private/etc/apache2/extra/sites/_base_ez5.conf

7. sudo mkdir /usr/local/bin

8. sudo cp addvhost /usr/local/bin/addvhost

9. sudo chmod +x /usr/local/bin/addvhost

## Usage
addvhost SITENAME

Now you can enter SITENAME.local - Voilá!